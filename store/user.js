import connection from '~/assets/js/connection';

const defaultState = {
    loading: false,
    pos: null,
    defaultPos: {
        center: [37.621192, 54.195725],
        zoom: 15.24,
        pitch: 59,
        bearing: -7.3
    }
};

export const state = () => ({
    ...defaultState,
    role: "",
    auth: false
});


export const mutations = {
    SET_LOGIN(state, data) {
        state.login = data;
    },
    SET_TOKEN(state, data) {
        state.token = data;
    },
    SET_EXPIRE(state, data) {
        state.expire = data;
    },
    SET_POS(state, data) {
        state.pos = data;
    },
    SET_LOADING(state, data) {
        state.loading = data;
    },
    CLEAR_DATA_STORE(state) {
        Object.keys(defaultState).forEach(key => {
            state[key] = defaultState[key]
        })
    },
    SET_ROLE(state, data) {
        state.role = data;
    },
    SET_AUTH(state, data) {
        state.auth = data;
    }
};

export const actions = {
    async auth({ commit }, body) {
        commit('CLEAR_DATA_STORE');
        localStorage.removeItem('login');
        localStorage.removeItem('token');
        localStorage.removeItem('expire');
        commit('SET_LOADING', true);
        const response = await this.$axios.post(
            connection.getAddress(`doauth`), body
        ).catch(function(error) {
            return error;
        });
        commit('SET_LOADING', false);
        if (response.data) {
            localStorage.setItem('login', body.username);
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('expire', response.data.expire);
            return null;
        }
        else {
            if (response.toString().includes("401")) {
                return "Неправильный логин или пароль";
            }
            else {
                return "Ошибка при авторизации";
            }
        }
    },
    clearUserStore({ commit }) {
        commit('CLEAR_DATA_STORE')
    },
    async verifyRole({commit}, token) {
        try {
            console.log('Making role check request...');
            let res = await fetch(connection.getAddress(`whoami?token=${token}`), {method: "GET", headers: {'Content-Type': 'application/json'}});
            if (res.status === '403' || res.status === '401') throw new Error(res.status);
            let data = await res.json();
            let { role } = data;
            commit('SET_ROLE', role)
        } catch (err) {
            if (err.message == '403' || err.message == '401') {
                commit('CLEAR_DATA_STORE');
                commit('SET_AUTH', false);
            }
        }
    },
    async logOut({ commit, dispatch }) {
        localStorage.setItem('token', '');
        let token = localStorage.getItem('token');
        await dispatch('verifyRole', token);
    }
};
