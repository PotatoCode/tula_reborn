const defaultState = {
  Clock: {
    show: false,
    clockID: null,
    date: null,
    month: null,
    year: null,
    hours: null,
    minutes: null,
    seconds: null,
  }
};

export const state = () => ({
  ...defaultState,
});

export const mutations = {
  CHANGE_STATE(state, params) {
    state[params.component][params.variable] = params.data;
  },
  CLEAR_DATA_STORE(state) {
    Object.keys(defaultState).forEach(key => {
        state[key] = defaultState[key]
    })
},
}