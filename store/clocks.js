// Циферблат
import connection from '~/assets/js/connection';

const defaultState = {
    loading: false,
    faces: [],
    hour_hand: [],
    minute_hand: [],
    centroid: [],
    centroid_buffer: [],
};

export const state = () => ({
    ...defaultState
});


export const mutations = {
    SET_DATA(state, data) {
        state.faces = data;
    },
    SET_HOUR_HAND(state, data) {
        state.hour_hand = data;
    },
    SET_MINUTE_HAND(state, data) {
        state.minute_hand = data;
    },
    SET_CENTROID(state, data) {
        state.centroid = data;
    },
    SET_CENTROID_BUFFER(state, data) {
        state.centroid_buffer = data;
    },
    SET_LOADING(state, data) {
        state.loading = data;
    },
    CLEAR_DATA_STORE(state) {
        Object.keys(defaultState).forEach(key => {
            state[key] = defaultState[key]
        })
    }
};

export const actions = {
    async getClocksData({ commit }) {
        console.log("event: getClocksData()");
        commit('SET_LOADING', true);
        const { data } = await this.$axios.get(
            connection.getAddress(`clocks/geojson?token=${this.state.user.token}`)
        ).catch(function(error) {
            if (!error.status) {
                return {};
            }
        });
        let faces = {
            type: "FeatureCollection",
            features: []
        };

        data.faces.forEach(element => {
            faces.features.push({
                'type': "Feature",
                'id': element.id,
                'properties': {},
                'geometry': element.geom
            });
        });

        let hour = {
            type: "FeatureCollection",
            features: [{
                'type': "Feature",
                'id': data.hour.id,
                'properties': {},
                'geometry': data.hour.geom
            }]
        };

        let minute = {
            type: "FeatureCollection",
            features: [{
                'type': "Feature",
                'id': data.minute.id,
                'properties': {},
                'geometry': data.minute.geom
            }]
        };

        let centroid = {
            type: "FeatureCollection",
            features: [{
                'type': "Feature",
                'id': data.centroid.id,
                'properties': {},
                'geometry': data.centroid.geom
            }]
        };

        let centroidBuffer = {
            type: "FeatureCollection",
            features: [{
                'type': "Feature",
                'id': data.centroid.id,
                'properties': {},
                'geometry': data.centroid.geomBuffer
            }]
        };

        const decodedData = {
            faces: faces,
            hour_hand: hour,
            minute_hand: minute,
            centroid: centroid,
            centroid_buffer: centroidBuffer
        };
        commit('SET_DATA', decodedData);
        commit('SET_LOADING', false);
    },
    clearClocksStore({ commit }) {
        commit('CLEAR_DATA_STORE')
    }
};
