import { mapActions } from "vuex";
import * as turf from '@turf/turf'

export const clocks = {
  data() {
    return {
      minuteDegree: null,
      hourDegree: null,
      smooth: true,
      clockID: null,
    }
  },
  methods: {
    ...mapActions({
      getClocksData: "clocks/getClocksData",
      clearClocksStore: "clocks/clearClocksStore"
    }),
    updateClock() {
      let sourceNameMinute = "source-clock-minute";
      let sourceNameHour = "source-clock-hour";
      let dataSourceHour = {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      };
      let dataSourceMinute = {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      };
      let tm = this.getTime();
      let minuteDegree;
      let hourDegree;
      if (this.smooth) {
        minuteDegree = 360 * (tm.secFromBegOfHour / (60*60));
        hourDegree = 360 * (tm.secFromBegOfDay / (12*60*60));
      }
      else {
        minuteDegree = tm.M * 6;
        hourDegree = (tm.H * 6 * 5) + 30 * (tm.M / 60);
      }
      if (this.minuteDegree !== minuteDegree || this.hourDegree !== hourDegree) {
        this.minuteDegree = minuteDegree;
        this.hourDegree = hourDegree;

        dataSourceHour.data = this.faces.hour_hand;
        let hourPolygon = turf.polygon(dataSourceHour.data.features[0].geometry.coordinates);
        dataSourceHour.data = turf.transformRotate(hourPolygon, hourDegree, {pivot: this.faces.centroid.features[0].geometry.coordinates});
        this.map.getSource(sourceNameHour).setData(dataSourceHour.data);

        dataSourceMinute.data = this.faces.minute_hand;
        let minutePolygon = turf.polygon(dataSourceMinute.data.features[0].geometry.coordinates);
        dataSourceMinute.data = turf.transformRotate(minutePolygon, minuteDegree, {pivot: this.faces.centroid.features[0].geometry.coordinates});
        this.map.getSource(sourceNameMinute).setData(dataSourceMinute.data);
      }
    },
    async addClocks() {
      let sourceName = "source-clock-faces";
      let layerName = "layer-clock-faces";
      let dataSource = {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      };
      
      let sourceNameHour = "source-clock-hour";
      let layerNameHour = "layer-clock-hour";
      let dataSourceHour = {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      };

      let sourceNameMinute = "source-clock-minute";
      let layerNameMinute = "layer-clock-minute";
      let dataSourceMinute = {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      };

      let sourceNameCentroid = "source-clock-centroid";
      let layerNameCentroid = "layer-clock-centroid";
      let dataSourceCentroid = {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      };


      if (!this.map.getSource(sourceName)) {
        await this.getClocksData();
        dataSource.data = this.faces.faces;
        this.map.addSource(sourceName, dataSource);

        let tm = this.getTime();
        let minuteDegree;
        let hourDegree;
        if (this.smooth) {
          minuteDegree = 360 * (tm.secFromBegOfHour / (60*60));
          hourDegree = 360 * (tm.secFromBegOfDay / (24*60*60));
        }
        else {
          minuteDegree = tm.M * 6;
          hourDegree = (tm.H * 6 * 5) + 30 * (tm.M / 60);
        }
        this.minuteDegree = minuteDegree;
        this.hourDegree = hourDegree;
        dataSourceCentroid.data = this.faces.centroid_buffer;
        this.map.addSource(sourceNameCentroid, dataSourceCentroid);

        dataSourceHour.data = this.faces.hour_hand;
        let hourPolygon = turf.polygon(dataSourceHour.data.features[0].geometry.coordinates);
        dataSourceHour.data = turf.transformRotate(hourPolygon, hourDegree, {pivot: this.faces.centroid.features[0].geometry.coordinates});
        this.map.addSource(sourceNameHour, dataSourceHour);
        

        dataSourceMinute.data = this.faces.minute_hand;
        let minutePolygon = turf.polygon(dataSourceMinute.data.features[0].geometry.coordinates);
        dataSourceMinute.data = turf.transformRotate(minutePolygon, minuteDegree, {pivot: this.faces.centroid.features[0].geometry.coordinates});
        this.map.addSource(sourceNameMinute, dataSourceMinute);

        // let summary = {
        //   'type': "FeatureCollection",
        //   'features': []
        // };
        // for (let i = 1; i < 61; i++) {
        //   let rotateMinute = turf.transformRotate(minutePolygon, i * 6, {pivot: dataSourceMinute.data.features[0].properties.pivot.coordinates});
        //   summary.features.push(rotateMinute);
        // }

      }
      
      if (!this.map.getLayer(layerName)) {
          this.map.addLayer({
            id: layerName,
            type: "fill-extrusion",
            source: sourceName,
            paint: {
              'fill-extrusion-opacity': 1.0,
              'fill-extrusion-color': '#73dae8',
              'fill-extrusion-vertical-gradient':false,
              'fill-extrusion-height': 28,
              'fill-extrusion-base': 23
            }
          });

          this.map.addLayer({
            id: layerNameHour,
            type: "fill-extrusion",
            source: sourceNameHour,
            paint: {
              'fill-extrusion-opacity': 1.0,
              'fill-extrusion-color': '#73dae8',
              'fill-extrusion-vertical-gradient':false,
              'fill-extrusion-height': 28,
              'fill-extrusion-base': 23
            }
          });

          this.map.addLayer({
            id: layerNameMinute,
            type: "fill-extrusion",
            source: sourceNameMinute,
            paint: {
              'fill-extrusion-opacity': 1.0,
              'fill-extrusion-color': '#73dae8',
              'fill-extrusion-vertical-gradient':false,
              'fill-extrusion-height': 28,
              'fill-extrusion-base': 23
            }
          });

          this.map.addLayer({
            id: layerNameCentroid,
            type: "fill-extrusion",
            source: sourceNameCentroid,
            paint: {
              'fill-extrusion-opacity': 1.0,
              'fill-extrusion-color': '#73dae8',
              'fill-extrusion-vertical-gradient':false,
              'fill-extrusion-height': 28,
              'fill-extrusion-base': 23
            }
          });
      }

      this.updateClock();
      this.clockID = setInterval(this.updateClock, 250);
      console.log("Слой часов включён");
    },
    removeClocks() {
        if (this.map.getLayer("layer-clock-faces")) {
          this.map.removeLayer("layer-clock-faces");
          this.map.removeLayer("layer-clock-hour");
          this.map.removeLayer("layer-clock-minute");
          this.map.removeLayer("layer-clock-centroid");
        }
        clearInterval(this.clockID);
        console.log("Слой часов отключён");
    },
    getTime() {
      let date = new Date();
      let h = date.getHours();
      let m = date.getMinutes();
      if (h == 0) {
        h = 12;
      }
      if (h > 12) {
        h = h - 12;
      }
      let begOfHour = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours());
      let begOfDay = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      return {
        H: h,
        M: m,
        secFromBegOfHour: parseInt((((date.getTime() - begOfHour.getTime()) / 1000).toFixed(0))),
        secFromBegOfDay: parseInt((((date.getTime() - begOfDay.getTime()) / 1000).toFixed(0)))
      };
    }
  },
  computed: {
    faces() {
    //   console.log("faces computed", this.$store.state);
      return this.$store.state.clocks.faces;
    },
    hour_hand() {
      //   console.log("hour_hand computed", this.$store.state);
        return this.$store.state.clocks.hour_hand;
    },
    minute_hand() {
      //   console.log("minute_hand computed", this.$store.state);
        return this.$store.state.clocks.minute_hand;
    },
    centroid() {
      //   console.log("centroid computed", this.$store.state);
        return this.$store.state.clocks.centroid;
    },
    centroid_buffer() {
      //   console.log("centroid_buffer computed", this.$store.state);
        return this.$store.state.clocks.centroid_buffer;
    }
  },
  beforeDestroy() {
    clearInterval(this.clockID);
  }
};
