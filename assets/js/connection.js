export default {
  url: {
    local: 'http://212.12.20.198:7938/tula',
    global: 'https://niikeeper.com/tula'
  },
  getAddress(path = "") {
    const base = this.url[process.env.mode].replace(/\/$/, "");
    return `${base}/${path.replace(/^\//, "")}`;
  }
}